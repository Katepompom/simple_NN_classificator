import os
from PIL import Image
import numpy as np
import cv2
from skimage.transform import resize
import shutil
import random

def image2vector(image, num_px):
#   num_px - expected size of image [num_px, num_px] 
    v = resize(image, (num_px,num_px))
    
    return v

def images2data(directory, num_px = 228)
#   directory = r'Panda data\panda\\'
#   directory = r'Panda data\not panda\\'
#   num_px - expected size of image [num_px, num_px] 
    
    X = []
    
    for filename in os.listdir(directory):
        image_file = Image.open(directory + filename)
        image = np.asarray(image_file)
        try:
            print(image.shape[2])
        except:
            image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
            print(filename)
        X.append(image2vector(image, num_px))
        
    return X

def pick_random_images_from_folder(n, src_folder, dst_folder)
#   dst_folder = r"C:\Users\Kate\Python_for_data_science\Pet projects\Panda classificator\Panda data\not panda\\"
#   src_folder = r"D:\data science course\deep learning\random images\train2014\\"
#   n - number of images to pick

    for i in range(n):
        file = random.choice(os.listdir(r"D:\data science course\deep learning\random images\train2014"))
        src = src_folder + file
        shutil.copy(src, dst_folder)